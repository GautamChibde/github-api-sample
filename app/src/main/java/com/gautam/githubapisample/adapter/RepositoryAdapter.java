package com.gautam.githubapisample.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gautam.githubapisample.R;
import com.gautam.githubapisample.activity.DetailActivity;
import com.gautam.githubapisample.model.net.repos.Repositories;
import com.gc.comman.util.Constant;
import com.gc.comman.util.PlatformUtil;

import java.util.List;

/**
 * Created by gautam on 7/11/16.
 */
public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryHolder> {

    private List<Repositories> repositories;
    private Context context;

    public RepositoryAdapter(List<Repositories> repositories, Context context) {
        this.repositories = repositories;
        this.context = context;
    }

    @Override
    public RepositoryAdapter.RepositoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RepositoryHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_repository, parent, false));
    }

    @Override
    public void onBindViewHolder(RepositoryAdapter.RepositoryHolder holder, final int position) {
        holder.name.setText(repositories.get(position).getName());
        holder.description.setText(repositories.get(position).getDescription());
        if (repositories.get(position).getStargazers_count() != null ||
                repositories.get(position).getStargazers_count() != 0) {
            holder.stars.setText(String.valueOf(repositories.get(position).getStargazers_count()));
        }else {
            holder.stars.setText(String.valueOf(0));
        }
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailActivity.class);
                i.putExtras(PlatformUtil.bundleParcelable(Constant.REPO, repositories.get(position)));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public static class RepositoryHolder extends RecyclerView.ViewHolder {

        private TextView name,  description, stars;
        private CardView cv;

        public RepositoryHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            description = (TextView) itemView.findViewById(R.id.tv_description);
            stars = (TextView) itemView.findViewById(R.id.tv_stars);
            cv = (CardView) itemView.findViewById(R.id.cv_container);
        }
    }
}
