package com.gautam.githubapisample.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gautam.githubapisample.R;
import com.gautam.githubapisample.model.net.commit.Sha;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by gautam on 7/11/16.
 */
public class CommitsAdapter extends RecyclerView.Adapter<CommitsAdapter.CommitsHolder> {
    private List<Sha> items;

    public CommitsAdapter(List<Sha> items) {
        this.items = items;
    }

    @Override
    public CommitsAdapter.CommitsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommitsHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_commits, parent, false));
    }

    @Override
    public void onBindViewHolder(CommitsAdapter.CommitsHolder holder, int position) {
        holder.commit.setText(items.get(position).getSha());
        holder.author.setText(items.get(position).getCommit().getAuthor().getName());
        holder.email.setText(items.get(position).getCommit().getAuthor().getEmail());
        SimpleDateFormat fmt = new SimpleDateFormat("ccc dd LLL yyyy hh:mm aa", Locale.getDefault());
        holder.date.setText(
                fmt.format(items.get(position).getCommit().getAuthor().getDate()).toUpperCase());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class CommitsHolder extends RecyclerView.ViewHolder {
        private TextView commit, author, date,email;

        public CommitsHolder(View itemView) {
            super(itemView);
            commit = (TextView) itemView.findViewById(R.id.commit);
            author = (TextView) itemView.findViewById(R.id.tv_name);
            date = (TextView) itemView.findViewById(R.id.date);
            email = (TextView) itemView.findViewById(R.id.tv_email);
        }
    }
}
