package com.gautam.githubapisample.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gautam.githubapisample.R;
import com.gautam.githubapisample.model.net.user.User;
import com.gc.comman.util.PlatformUtil;

import java.util.List;

/**
 * Created by gautam on 11/11/16.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserHolder> {
    private Context context;
    private List<User> items;

    public UserAdapter(List<User> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user, parent, false));
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        PlatformUtil.setImagePicasso(
                context,
                items.get(position).getProfileImage(),
                holder.image,
                R.drawable.noimage,
                R.drawable.progress_image);
        holder.name.setText(items.get(position).getLogin());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class UserHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;

        public UserHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.iv_image);
            name = (TextView) itemView.findViewById(R.id.tv_name);
        }
    }
}
