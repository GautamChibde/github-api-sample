package com.gautam.githubapisample.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gautam.githubapisample.ApiInterface;
import com.gautam.githubapisample.R;
import com.gautam.githubapisample.fragment.RepositoriesFragment;
import com.gautam.githubapisample.fragment.UserListDialog;
import com.gautam.githubapisample.model.net.repos.Repositories;
import com.gautam.githubapisample.model.net.user.User;
import com.gc.comman.App;
import com.gc.comman.infra.Resource;
import com.gc.comman.ui.BaseActivity;
import com.gc.comman.util.Constant;
import com.gc.comman.util.PlatformUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

@SuppressWarnings("unused,unchecked")
public class ProfileActivity extends BaseActivity {
    private Call<List<Repositories>> repositoryCall;
    private Call<List<User>> userCall;
    private ApiInterface apiService;
    private String user;
    private User auth;
    private List<Repositories> repositories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progress.show();
        if (getIntent().getExtras() != null) {
            auth = getIntent().getExtras().getParcelable(Constant.GIT_AUTH);
            user = auth.getLogin();
        }
        setTitle(user);
        ImageView profileImage = (ImageView) findViewById(R.id.iv_profile_image);
        initBio();
        init();
        PlatformUtil.setImagePicasso(
                this,
                auth.getProfileImage(),
                profileImage,
                R.drawable.noimage,
                R.drawable.progress_animation);

        apiService = App.getClient().create(ApiInterface.class);
        repositoryCall = apiService.getRepos(auth.getLogin(), getApp().getCredentials());
        postServer();
    }

    private void initBio() {
        TextView company = (TextView) findViewById(R.id.tv_company);
        TextView email = (TextView) findViewById(R.id.tv_email);
        TextView location = (TextView) findViewById(R.id.tv_location);
        if (auth.getCompany() != null) {
            company.setText(auth.getCompany());
        } else {
            company.setText(String.valueOf("N/A"));
        }
        if (auth.getEmail() != null) {
            email.setText(auth.getEmail());
        } else {
            email.setText(String.valueOf("N/A"));
        }
        if (auth.getLocation() != null) {
            location.setText(auth.getLocation());
        } else {
            location.setText(String.valueOf("N/A"));
        }
    }

    private void init() {
        TextView followers = (TextView) findViewById(R.id.tv_followers);
        TextView following = (TextView) findViewById(R.id.tv_following);
        followers.setText(auth.getFollowers());
        following.setText(auth.getFollowing());
    }

    private void postServer() {
        Resource.enquire(repositoryCall, Constant.REPOSITORIES_REQUEST_CODE);
    }

    private void initTabs() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(new FragmentStatePagerAdapter(getFragmentManager()) {
            private String[] tabs = {"Repositories", "Forks"};

            @Override
            public Fragment getItem(int position) {
                Fragment fragment = new RepositoriesFragment();
                Bundle args = new Bundle();
                switch (position) {
                    case 0:
                        args.putParcelableArrayList(
                                Constant.REPOSITORIES,
                                (ArrayList<? extends Parcelable>)
                                        getFilteredRepositories(repositories, false));
                        fragment.setArguments(args);
                        break;
                    case 1:
                        args.putParcelableArrayList(
                                Constant.REPOSITORIES,
                                (ArrayList<? extends Parcelable>)
                                        getFilteredRepositories(repositories, true));
                        fragment.setArguments(args);
                        break;
                }
                return fragment;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return tabs[position];
            }

            @Override
            public int getCount() {
                return tabs.length;
            }
        });
        tabLayout.setupWithViewPager(pager);
    }

    private List<Repositories> getFilteredRepositories(
            List<Repositories> repositories,
            boolean clause) {
        List<Repositories> repo = new ArrayList<>();
        for (Repositories r : repositories) {
            if (r.getForked() == clause) {
                repo.add(r);
            }
        }
        return repo;
    }

    public void onEvent(Resource.HttpRequestComplete event) {
        progress.dismiss();
        Bundle b = new Bundle();
        UserListDialog userListDialog = new UserListDialog();
        if (event.isSuccessful()) {
            if (event.getRequestId() == Constant.REPOSITORIES_REQUEST_CODE) {
                findViewById(R.id.pb).setVisibility(View.GONE);
                repositories = (List<Repositories>) event.getResponse();
                initTabs();
            } else if (event.getRequestId() == Constant.FOLLOWER_REQUEST_CODE) {
                List<User> user = (List<User>) event.getResponse();
                b.putParcelableArrayList(Constant.USER, (ArrayList<? extends Parcelable>) user);
                b.putString(Constant.TITLE, "Followers");
                userListDialog.setArguments(b);
                userListDialog.show(getFragmentManager(), "user dialog");
            } else if (event.getRequestId() == Constant.FOLLOWING_REQUEST_CODE) {
                List<User> user = (List<User>) event.getResponse();
                b.putParcelableArrayList(Constant.USER, (ArrayList<? extends Parcelable>) user);
                b.putString(Constant.TITLE, "Following");
                userListDialog.setArguments(b);
                userListDialog.show(getFragmentManager(), "user dialog");
            }
        } else {
            showDialog(getResources().getString(R.string.network_error));
        }
    }

    public void followers(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        progress.show();
        userCall = apiService.getFollowers(user, getApp().getCredentials());
        Resource.enquire(userCall, Constant.FOLLOWER_REQUEST_CODE);
    }

    public void following(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        progress.show();
        userCall = apiService.getFollowing(user, getApp().getCredentials());
        Resource.enquire(userCall, Constant.FOLLOWING_REQUEST_CODE);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_profile;
    }

    @Override
    protected int getTitleId() {
        return R.id.tvTitle;
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }
}
