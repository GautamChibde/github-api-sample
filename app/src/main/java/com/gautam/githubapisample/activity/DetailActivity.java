package com.gautam.githubapisample.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.TextView;

import com.gautam.githubapisample.ApiInterface;
import com.gautam.githubapisample.R;
import com.gautam.githubapisample.fragment.CommitListFragment;
import com.gautam.githubapisample.fragment.UserListFragment;
import com.gautam.githubapisample.model.net.commit.Sha;
import com.gautam.githubapisample.model.net.repos.Repositories;
import com.gautam.githubapisample.model.net.user.User;
import com.gc.comman.App;
import com.gc.comman.infra.Resource;
import com.gc.comman.ui.BaseActivity;
import com.gc.comman.util.Constant;
import com.gc.comman.util.PlatformUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

@SuppressWarnings("unused,unchecked")
public class DetailActivity extends BaseActivity {
    private List<Sha> commits;
    private List<User> collaborators;
    private boolean fragmentSelector = true;
    private FloatingActionButton fb;
    private Call<List<Sha>> callCommits;
    private Call<List<User>> callCollaborators;
    private TextView subTitle;
    private boolean noCommits = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Repositories repo = getIntent().getExtras().getParcelable(Constant.REPO);
        setTitle(repo.getName());
        ApiInterface apiService = App.getClient().create(ApiInterface.class);
        subTitle = (TextView) findViewById(R.id.tvSubTitle);
        subTitle.setText(String.valueOf("Commits"));
        callCommits = apiService.getCommits(
                repo.getOwner().getLogin(),
                repo.getName(),
                getApp().getCredentials());
        callCollaborators = apiService.getCollaborators(
                repo.getOwner().getLogin(),
                repo.getName(),
                getApp().getCredentials());

        fb = (FloatingActionButton) findViewById(R.id.fb_options);
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragmentSelector) {
                    fb.setImageResource(R.drawable.ic_people_black_24dp);
                    postServerCommits();
                } else {
                    fb.setImageResource(R.drawable.ic_git_commit);
                    postServerCollaborators();
                }
            }
        });
        postServerCommits();
    }

    private void postServerCommits() {
        progress.show();
        if ((commits == null || commits.size() == 0) && !noCommits) {
            Resource.enquire(callCommits, Constant.COMMIT_REQUEST_CODE);
        } else {
            initFragment();
        }
    }

    private void postServerCollaborators() {
        progress.show();
        if (collaborators == null || collaborators.size() == 0) {
            Resource.enquire(callCollaborators, Constant.COLLABORATORS_REQUEST_CODE);
        } else {
            initFragment();
        }
    }

    public void onEvent(Resource.HttpRequestComplete event) {
        progress.dismiss();
        if (event.isSuccessful()) {
            if (event.getRequestId() == Constant.COLLABORATORS_REQUEST_CODE) {
                collaborators = (List<User>) event.getResponse();
                initFragment();
            }
            if (event.getRequestId() == Constant.COMMIT_REQUEST_CODE) {
                commits = (List<Sha>) event.getResponse();
                initFragment();
            }
        } else {
            showDialog(getResources().getString(R.string.network_error));
        }
    }

    public void onEvent(CommitListFragment.RepoHasNoCommits event) {
        noCommits = true;
    }

    private void initFragment() {
        progress.dismiss();
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, getFragment())
                .commit();
    }

    public Fragment getFragment() {
        Fragment fragment;
        if (fragmentSelector) {
            fragmentSelector = false;
            subTitle.setText(String.valueOf("Commits"));
            fragment = new CommitListFragment();
            fragment.setArguments(PlatformUtil.bundleParcelableArrayList(
                    Constant.COMMITS,
                    (ArrayList<? extends Parcelable>) commits));
        } else {
            fragmentSelector = true;
            subTitle.setText(String.valueOf("Collaborators"));
            fragment = new UserListFragment();
            fragment.setArguments(PlatformUtil.bundleParcelableArrayList(
                    Constant.COLLABORATORS,
                    (ArrayList<? extends Parcelable>) collaborators));
        }
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_detail;
    }

}
