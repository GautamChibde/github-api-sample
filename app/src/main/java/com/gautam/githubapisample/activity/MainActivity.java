package com.gautam.githubapisample.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.gautam.githubapisample.ApiInterface;
import com.gautam.githubapisample.R;
import com.gautam.githubapisample.model.db.SavedUser;
import com.gautam.githubapisample.model.net.user.User;
import com.gc.comman.App;
import com.gc.comman.infra.Resource;
import com.gc.comman.ui.BaseActivity;
import com.gc.comman.util.Constant;
import com.gc.comman.util.PlatformUtil;

import retrofit2.Call;

@SuppressWarnings("unused,unchecked")
public class MainActivity extends BaseActivity {
    private Call<User> call;
    private ApiInterface apiService;
    private AutoCompleteTextView userName;
    private EditText password;
    private String basicAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getResources().getString(R.string.app_name));
        apiService = App.getClient().create(ApiInterface.class);
        userName = (AutoCompleteTextView) findViewById(R.id.et_user_name);
        password = (EditText) findViewById(R.id.et_password);
        try {
            userName.setAdapter(new ArrayAdapter<>
                    (this, android.R.layout.simple_list_item_1, SavedUser.getSavedUsers()));
        } catch (Exception e) {
            throw new RuntimeException("Disable the instant run and rebuild project");
        }
        userName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                password.requestFocus();
            }
        });
    }

    public void submit(View view) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 2000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        if (TextUtils.isEmpty(userName.getText())) {
            userName.requestFocus();
            userName.setError("Please enter username");
        } else if (TextUtils.isEmpty(password.getText())) {
            password.requestFocus();
            password.setError("Please enter password");
        } else {
            progress.show();
            String userCredentials =
                    userName.getText().toString() + ":" + password.getText().toString();
            basicAuth = "Basic " + new String(
                    Base64.encode(userCredentials.getBytes(), Base64.DEFAULT));
            call = apiService.getGitAuth(userName.getText().toString(), basicAuth.trim());
            postServer(Constant.AUTH_REQUEST_CODE);
        }
    }

    private void postServer(int requestCode) {
        if (!call.isExecuted()) {
            Resource.enquire(call, requestCode);
        } else {
            Resource.enquireClone(call, requestCode);
            progress.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userName.setText("");
        password.setText("");
        userName.requestFocus();
        userName.setAdapter(new ArrayAdapter<>
                (this, android.R.layout.simple_list_item_1, SavedUser.getSavedUsers()));
    }

    public void onEvent(Resource.HttpRequestComplete event) {
        progress.dismiss();
        if (event.isSuccessful()) {
            if (event.getResponse() != null) {
                if (event.getResponse() instanceof User) {
                    User auth = (User) event.getResponse();
                    saveToDb(auth);
                    startNewActivity(ProfileActivity.class,
                            PlatformUtil.bundleParcelable(Constant.GIT_AUTH, auth));
                }
            } else {
                invalidUserDialog("invalid username");
            }
        } else {
            networkErrorDialog("Problem with your network");
        }
    }

    /**
     * TODO need to work on getting auth tokens
     */
    private void saveToDb(User auth) {
        SavedUser savedUser = new SavedUser();
        savedUser.setId(auth.getId());
        savedUser.setName(auth.getLogin());
        savedUser.save();
        getApp().setCredentials(basicAuth.trim());
        //TODO bad implementation
//        SharedPreferences.Editor editor = getApp().getSharedPref().edit();
//        editor.putString(Constant.LOGGED_IN_USER, auth.getLogin());
//        editor.putString(Constant.BASIC_AUTH, basicAuth);
//        editor.apply();
    }

    private void invalidUserDialog(String message) {
        new AlertDialog.Builder(this)
                .setTitle("network error")
                .setMessage(message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_github_circle_black_24dp)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .show();
    }

    private void networkErrorDialog(String message) {
        new AlertDialog.Builder(this)
                .setTitle("network error")
                .setMessage(message)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        postServer(Constant.AUTH_REQUEST_CODE);
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_github_circle_black_24dp)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .show();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected int getTitleId() {
        return R.id.tvTitle;
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }
}
