package com.gautam.githubapisample.model.net.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserPlan implements Parcelable {

    private long collaborators;
    @SerializedName("private_repos")
    public long privateRepos;
    private long space;
    private String name;

    public UserPlan() {
    }

    protected UserPlan(Parcel in) {
        this.collaborators = in.readLong();
        this.privateRepos = in.readLong();
        this.space = in.readLong();
        this.name = in.readString();
    }

    public static final Creator<UserPlan> CREATOR = new Creator<UserPlan>() {
        @Override
        public UserPlan createFromParcel(Parcel in) {
            return new UserPlan(in);
        }

        @Override
        public UserPlan[] newArray(int size) {
            return new UserPlan[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.collaborators);
        dest.writeLong(this.privateRepos);
        dest.writeLong(this.space);
        dest.writeString(this.name);
    }

    @Override
    public String toString() {
        return "UserPlan{" +
                "collaborators=" + collaborators +
                ", privateRepos=" + privateRepos +
                ", space=" + space +
                ", name='" + name + '\'' +
                '}';
    }

    public long getCollaborators() {
        return collaborators;
    }

    public void setCollaborators(long collaborators) {
        this.collaborators = collaborators;
    }

    public long getPrivateRepos() {
        return privateRepos;
    }

    public void setPrivateRepos(long privateRepos) {
        this.privateRepos = privateRepos;
    }

    public long getSpace() {
        return space;
    }

    public void setSpace(long space) {
        this.space = space;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}