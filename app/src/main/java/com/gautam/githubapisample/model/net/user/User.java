package com.gautam.githubapisample.model.net.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by gautam on 6/11/16.
 */
public class User implements Parcelable {
    public String login;
    private String name;
    private String email;
    private Date date;
    private String message;
    @SerializedName("avatar_url")
    private String profileImage;
    private Long id;
    private String location;
    private String company;
    private String url;
    private String html_url;
    private String repos_url;
    private String followers;
    private String following;
    private String documentation_url;
    private UserPlan plan;

    public User() {
    }

    protected User(Parcel in) {
        login = in.readString();
        name = in.readString();
        email = in.readString();
        long tmp_date = in.readLong();
        date = tmp_date == -1 ? null : new Date(tmp_date);
        this.message = in.readString();
        this.profileImage = in.readString();
        this.id = in.readLong();
        this.location = in.readString();
        this.company = in.readString();
        this.url = in.readString();
        this.html_url = in.readString();
        this.repos_url = in.readString();
        this.followers = in.readString();
        this.following = in.readString();
        this.documentation_url = in.readString();
        this.plan = in.readParcelable(UserPlan.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(login);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeLong(date != null ? date.getTime() : -1);
        dest.writeString(message);
        dest.writeString(profileImage);
        dest.writeLong(id);
        dest.writeString(location);
        dest.writeString(company);
        dest.writeString(url);
        dest.writeString(html_url);
        dest.writeString(repos_url);
        dest.writeString(followers);
        dest.writeString(following);
        dest.writeString(documentation_url);
        dest.writeParcelable(plan, 0);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", date=" + date +
                ", message='" + message + '\'' +
                ", profileImage='" + profileImage + '\'' +
                ", id=" + id +
                ", location='" + location + '\'' +
                ", company='" + company + '\'' +
                ", url='" + url + '\'' +
                ", html_url='" + html_url + '\'' +
                ", repos_url='" + repos_url + '\'' +
                ", followers='" + followers + '\'' +
                ", following='" + following + '\'' +
                ", documentation_url='" + documentation_url + '\'' +
                ", plan=" + plan +
                '}';
    }

    public String getDocumentation_url() {
        return documentation_url;
    }

    public void setDocumentation_url(String documentation_url) {
        this.documentation_url = documentation_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getRepos_url() {
        return repos_url;
    }

    public void setRepos_url(String repos_url) {
        this.repos_url = repos_url;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}