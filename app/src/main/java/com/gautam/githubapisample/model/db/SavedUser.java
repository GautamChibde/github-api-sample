package com.gautam.githubapisample.model.db;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gautam on 8/11/16.
 */
@Table(name = "saved_user")
public class SavedUser implements Serializable {
    private static final Long serialVersionUID = 1L;
    private Long id;
    @Column(name = "name")
    private String name;

    public void save() {
        List<SavedUser> user = SugarRecord.find(
                SavedUser.class,
                "id = ?",
                String.valueOf(this.getId()));
        if (user.size() == 0) {
            SugarRecord.save(this);
        }
    }

    public SavedUser() {
    }

    public static List<String> getSavedUsers() {
        List<String> toReturn = new ArrayList<>();
        for (SavedUser u : SugarRecord.listAll(SavedUser.class)) {
            toReturn.add(u.getName());
        }
        return toReturn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SavedUser{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
