package com.gautam.githubapisample.model.net.commit;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by gautam on 6/11/16.
 */
public class Sha implements Parcelable {
    private String sha;
    private Commit commit;
    private String url;

    public Sha() {
    }

    protected Sha(Parcel in) {
        sha = in.readString();
        commit = in.readParcelable(Commit.class.getClassLoader());
        url = in.readString();
    }

    public static final Creator<Sha> CREATOR = new Creator<Sha>() {
        @Override
        public Sha createFromParcel(Parcel in) {
            return new Sha(in);
        }

        @Override
        public Sha[] newArray(int size) {
            return new Sha[size];
        }
    };

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public Commit getCommit() {
        return commit;
    }

    @Override
    public String toString() {
        return "Sha{" +
                "sha='" + sha + '\'' +
                ", commit=" + commit +
                ", url='" + url + '\'' +
                '}';
    }

    public void setCommit(Commit commit) {
        this.commit = commit;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sha);
        dest.writeParcelable(commit, flags);
        dest.writeString(url);
    }
}
