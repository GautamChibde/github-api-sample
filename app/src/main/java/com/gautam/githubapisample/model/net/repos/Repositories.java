package com.gautam.githubapisample.model.net.repos;

import android.os.Parcel;
import android.os.Parcelable;

import com.gautam.githubapisample.model.net.user.User;
import com.google.gson.annotations.SerializedName;

/**
 * Created by gautam on 6/11/16.
 */
public class Repositories implements Parcelable {
    private Long id;
    private String name;
    private User owner;
    private String fullName;
    private String description;
    private Long stargazers_count;
    @SerializedName("fork")
    private Boolean isForked;
    @SerializedName("private")
    private Boolean isPrivate;

    public Repositories() {
    }

    protected Repositories(Parcel in) {
        id = in.readLong();
        name = in.readString();
        owner = in.readParcelable(User.class.getClassLoader());
        fullName = in.readString();
        description = in.readString();
        stargazers_count = in.readLong();
        isForked = in.readByte() != 0;
        isPrivate = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeParcelable(owner, 0);
        dest.writeString(fullName);
        dest.writeString(description);
        dest.writeLong(stargazers_count);
        dest.writeByte(isForked ? (byte) 1 : (byte) 0);
        dest.writeByte(isPrivate ? (byte) 1 : (byte) 0);
    }

    public static final Creator<Repositories> CREATOR = new Creator<Repositories>() {
        public Repositories createFromParcel(Parcel in) {
            return new Repositories(in);
        }
        public Repositories[] newArray(int size) {
            return new Repositories[size];
        }
    };

    @Override
    public String toString() {
        return "Repositories{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                ", fullName='" + fullName + '\'' +
                ", description='" + description + '\'' +
                ", stargazers_count=" + stargazers_count +
                ", isForked=" + isForked +
                ", isPrivate=" + isPrivate +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Long getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(Long stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getFullName() {
        return fullName;
    }

    public Boolean getForked() {
        return isForked;
    }

    public void setForked(Boolean forked) {
        isForked = forked;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
