package com.gautam.githubapisample.model.net.commit;

import android.os.Parcel;
import android.os.Parcelable;

import com.gautam.githubapisample.model.net.user.User;

/**
 * Created by gautam on 6/11/16.
 */
public class Commit implements Parcelable {

    private User author;
    private User committer;
    private String message;
    private Tree tree;
    private String url;
    private Long comment_count;

    public Commit() {
    }

    protected Commit(Parcel in) {
        author = in.readParcelable(User.class.getClassLoader());
        committer = in.readParcelable(User.class.getClassLoader());
        message = in.readString();
        tree = in.readParcelable(Tree.class.getClassLoader());
        url = in.readString();
        comment_count = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(author, 0);
        dest.writeParcelable(committer, 0);
        dest.writeString(message);
        dest.writeParcelable(tree, 0);
        dest.writeString(url);
        dest.writeLong(comment_count);
    }

    public static final Creator<Commit> CREATOR = new Creator<Commit>() {
        public Commit createFromParcel(Parcel in) {
            return new Commit(in);
        }

        public Commit[] newArray(int size) {
            return new Commit[size];
        }
    };

    @Override
    public String toString() {
        return "Commit{" +
                "author=" + author +
                ", committer=" + committer +
                ", message='" + message + '\'' +
                ", tree=" + tree +
                ", url='" + url + '\'' +
                ", comment_count=" + comment_count +
                '}';
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Long getComment_count() {
        return comment_count;
    }

    public void setComment_count(Long comment_count) {
        this.comment_count = comment_count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getCommitter() {
        return committer;
    }

    public void setCommitter(User committer) {
        this.committer = committer;
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
