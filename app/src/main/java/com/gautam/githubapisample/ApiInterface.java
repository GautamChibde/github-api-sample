package com.gautam.githubapisample;

import com.gautam.githubapisample.model.net.commit.Sha;
import com.gautam.githubapisample.model.net.repos.Repositories;
import com.gautam.githubapisample.model.net.user.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by gautam on 6/11/16.
 */
public interface ApiInterface {
    @GET("users/{user}")
    Call<User> getGitAuth(
            @Path("user") String user,
            @Header("Authorization") String auth);

    @GET("users/{user}/repos")
    Call<List<Repositories>> getRepos(
            @Path("user") String user,
            @Header("Authorization") String auth);

    @GET("/users/{user}/following")
    Call<List<User>> getFollowing(
            @Path("user") String user,
            @Header("Authorization") String auth);

    @GET("/users/{user}/followers")
    Call<List<User>> getFollowers(
            @Path("user") String user,
            @Header("Authorization") String auth);

    @GET("/repos/{user}/{repo}/commits")
    Call<List<Sha>> getCommits(
            @Path("user") String user,
            @Path("repo") String repo,
            @Header("Authorization") String auth);

    @GET("/repos/{user}/{name}/collaborators")
    Call<List<User>> getCollaborators(
            @Path("user") String user,
            @Path("name") String repo,
            @Header("Authorization") String auth);
}
