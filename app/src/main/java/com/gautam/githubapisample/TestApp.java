package com.gautam.githubapisample;

import com.gc.comman.App;

public class TestApp extends App {
    public static final String API_URL = "https://api.github.com";
    @Override
    public void onCreate() {
        super.onCreate();
        setBaseUrl(API_URL);
        initLocalDb();
    }
}
