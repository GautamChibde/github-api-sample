package com.gautam.githubapisample.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gautam.githubapisample.R;
import com.gautam.githubapisample.adapter.RepositoryAdapter;
import com.gautam.githubapisample.model.net.repos.Repositories;
import com.gc.comman.ui.BaseFragment;
import com.gc.comman.util.Constant;

import java.util.List;

public class RepositoriesFragment extends BaseFragment {

    protected void initView(View v) {
        List<Repositories> repositories = getArguments().getParcelableArrayList(Constant.REPOSITORIES);
        if (repositories.size() != 0) {
            RecyclerView rv = (RecyclerView) v.findViewById(R.id.rv);
            rv.setLayoutManager(new LinearLayoutManager(getActivity()));
            rv.setAdapter(new RepositoryAdapter(repositories, getActivity()));
        }else {
            setErrorText("You don't hav any repositories");
        }
    }

    @Override
    protected int getErrorView() {
        return R.id.tv_message;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recycler;
    }
}
