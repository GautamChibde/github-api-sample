package com.gautam.githubapisample.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gautam.githubapisample.R;
import com.gautam.githubapisample.adapter.UserAdapter;
import com.gautam.githubapisample.model.net.user.User;
import com.gc.comman.ui.BaseFragment;
import com.gc.comman.util.Constant;

import java.util.List;

public class UserListFragment extends BaseFragment {
    private List<User> users;

    @Override
    protected void initView(View v) {
        super.initView(v);
        if (getArguments() != null) {
            users = getArguments().getParcelableArrayList(Constant.COLLABORATORS);
        }
        RecyclerView user = (RecyclerView) v.findViewById(R.id.rv);
        user.setLayoutManager(new LinearLayoutManager(getActivity()));
        user.setAdapter(new UserAdapter(users, getActivity()));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recycler;
    }
}
