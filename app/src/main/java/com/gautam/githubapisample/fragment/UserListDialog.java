package com.gautam.githubapisample.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.gautam.githubapisample.R;
import com.gautam.githubapisample.adapter.UserAdapter;
import com.gautam.githubapisample.model.net.user.User;
import com.gc.comman.ui.AbstractDialogFragment;
import com.gc.comman.util.Constant;

import java.util.List;

/**
 * Created by gautam on 7/11/16.
 */
public class UserListDialog extends AbstractDialogFragment {
    private List<User> users;
    private String title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            users = getArguments().getParcelableArrayList(Constant.USER);
            title = getArguments().getString(Constant.TITLE);
        }
    }

    @Override
    protected void initView(View v) {
        setupToolbar(
                (Toolbar) v.findViewById(R.id.toolbar),
                title,
                R.drawable.ic_arrow_back_black_24dp,
                R.menu.menu_done,
                R.id.tvTitle,
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.actionDone:
                                dismiss();
                                return true;
                        }
                        return false;
                    }
                }
        );
        if (users.size() != 0) {
            RecyclerView user = (RecyclerView) v.findViewById(R.id.rv);
            user.setLayoutManager(new LinearLayoutManager(getActivity()));
            user.setAdapter(new UserAdapter(users, getActivity()));
        } else {
            if (title.equalsIgnoreCase("Followers")) {
                setErrorText("You don't have any Followers");
            } else {
                setErrorText("You are not following any one");
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.user_dialog;
    }

    @Override
    protected int getErrorView() {
        return R.id.tv_message;
    }
}
