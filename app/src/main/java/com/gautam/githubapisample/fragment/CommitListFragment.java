package com.gautam.githubapisample.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gautam.githubapisample.R;
import com.gautam.githubapisample.adapter.CommitsAdapter;
import com.gautam.githubapisample.model.net.commit.Sha;
import com.gc.comman.ui.BaseFragment;
import com.gc.comman.util.Constant;

import java.util.List;

import de.greenrobot.event.EventBus;

public class CommitListFragment extends BaseFragment {

    @Override
    protected void initView(View v) {
        super.initView(v);
        List<Sha> commits = getArguments().getParcelableArrayList(Constant.COMMITS);
        if (commits != null) {
            RecyclerView rv = (RecyclerView) v.findViewById(R.id.rv);
            rv.setLayoutManager(new LinearLayoutManager(getActivity()));
            rv.setAdapter(new CommitsAdapter(commits));
        } else {
            setErrorText("Looks like you don't have any commits");
            EventBus.getDefault().post(new RepoHasNoCommits());
        }

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recycler;
    }

    @Override
    protected int getErrorView() {
        return R.id.tv_message;
    }

    public static class RepoHasNoCommits {
    }
}
