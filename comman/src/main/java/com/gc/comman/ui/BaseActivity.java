package com.gc.comman.ui;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.comman.App;
import com.gc.comman.R;
import com.gc.comman.infra.NetworkManager;
import com.gc.comman.infra.ReflectionUtil;
import com.gc.comman.util.AppActivity;
import com.gc.comman.util.Constant;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Parent Activity for all activities in the application
 * Provides methods for working with most infrastructure
 **/
@SuppressWarnings("unused")
public abstract class BaseActivity extends AppCompatActivity implements AppActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();
    protected static long mLastClickTime = 0;
    protected final EventBus bus = EventBus.getDefault();
    protected Toolbar toolbar;
    protected TextView tvTitle;
    protected AccountManager accountManager;
    protected ProgressDialog progress;

    // Lifecycle methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountManager = AccountManager.get(getBaseContext());
        if (getLayoutId() != 0) {
            setContentView(getLayoutId());
            setupDrawer();
            setupToolbar();
            replaceContent(getInitContent());
            setTitle("");
        } else {
            if (getInitContent() != null) {
                Log.d(TAG, "Override getLayoutId() to set layout");
            }
        }
        initProgressBar();
    }

    private void initProgressBar() {
        progress = new ProgressDialog(this);
        progress.setMessage("Loading...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setProgress(0);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (hasDrawer()) {
            post(new NavigationDrawer.SyncDrawerEvent());
        }
    }

    @Override
    public void onBackPressed() {
        if (null != getDrawerLayout() &&
                getDrawerLayout().isDrawerOpen(GravityCompat.START)) {
            getDrawerLayout().closeDrawer(GravityCompat.START);
        } else {
            if (!getFragmentManager().popBackStackImmediate()) {
                supportFinishAfterTransition();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public boolean hasClientToken() {
        return getApp().getFromPref(Constant.KEY_CLIENT_TOKEN) != null;
    }

    public String getClientToken() {
        return getApp().getFromPref(Constant.KEY_CLIENT_TOKEN);
    }

    public void requestAuth(Class<? extends Activity> authActivityClass) {
        startActivityForResult(new Intent(this, authActivityClass), Constant.AUTH_REQUEST_CODE);
    }

    public boolean isLoggedIn() {
        return getApp().getFromPref(Constant.KEY_USER_TOKEN) != null;
    }

    protected Long getLoggedInUserId() {
        return getApp().getSharedPref().getLong(Constant.KEY_USER_ID, 0);
    }

    public boolean isSuperAdmin() {
        return "super_admin".equals(getApp().getFromPref(Constant.KEY_USER_ROL));
    }

    public boolean isAdmin() {
        return "admin".equals(getApp().getFromPref(Constant.KEY_USER_ROL));
    }

    public void signOut() {
        getApp().saveToPref(Constant.KEY_USER_TOKEN, null);
    }

    // UI

    protected boolean viewExists(int id) {
        return findViewById(id) != null;
    }

    protected boolean hasDrawer() {
        return viewExists(R.id.drawer);
    }

    protected boolean hasToolbar() {
        return viewExists(R.id.toolbar);
    }

    protected void setupDrawer() {
        int drawerId = getDrawerFragmentId();
        if (hasDrawer()) {
            bus.post(
                    new NavigationDrawer.SetupDrawerEvent(
                            getDrawerLayout(),
                            drawerId
                    )
            );
        }
    }

    public void onEvent(NetworkManager.NetworkStateChangedEvent event) {
        if (!event.isConnected()) {
            Toast.makeText(
                    this,
                    getResources().getString(R.string.no_network_message),
                    Toast.LENGTH_LONG).show();
        }
    }

    protected void setupToolbar() {
        if (hasToolbar()) {
            toolbar = (Toolbar) findViewById(getToolbarId());
            if (toolbar.findViewById(getTitleId()) != null) {
                tvTitle = (TextView) toolbar.findViewById(getTitleId());
            }
            setSupportActionBar(toolbar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null && displayHomeButton()) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeButtonEnabled(true);
            }
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        if (tvTitle != null) {
            super.setTitle("");
            tvTitle.setText(title);
        } else {
            super.setTitle(title);
        }
    }

    protected SlidingUpPanelLayout.PanelState onBackPanelState() {
        return SlidingUpPanelLayout.PanelState.HIDDEN;
    }

    protected Fragment getUpPanelFragment() {
        return null;
    }

    protected SlidingUpPanelLayout.PanelState getUpPanelState() {
        return SlidingUpPanelLayout.PanelState.HIDDEN;
    }

    protected void replaceContent(Class newFragmentClass, Bundle data) {
        try {
            Fragment frag = (Fragment) newFragmentClass.newInstance();
            frag.setArguments(data);
            replaceContent(frag, false);
        } catch (Exception e) {
            Log.d(TAG, "Got error: " + e.getMessage());
        }
    }

    protected void replaceContent(Fragment newFragment) {
        int containerId = getContentContainerId();
        if (containerId != 0 && newFragment != null) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(containerId, newFragment, newFragment.getClass().getName())
                    .commit();
        }
    }

    protected void replaceContent(Fragment newFragment, boolean addToBackStack) {
        if (addToBackStack) {
            int containerId = getContentContainerId();
            if (containerId != 0 && newFragment != null) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(containerId, newFragment, newFragment.getClass().getName())
                        .addToBackStack(newFragment.getClass().getName())
                        .commit();
            }
        } else {
            replaceContent(newFragment);
        }
    }

    private void replaceContent(Class newFragmentClass, Bundle data,
                                boolean addToBackStack) {
        try {
            Fragment frag = (Fragment) newFragmentClass.newInstance();
            frag.setArguments(data);
            replaceContent(frag, addToBackStack);
        } catch (Exception e) {
            Log.d(TAG, "Got error: " + e.getMessage());
        }
    }

    protected int getLayoutId() {
        return 0;
    }

    protected void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    protected int getDrawerFragmentId() {
        return R.id.drawer;
    }


    protected int getToolbarId() {
        return R.id.toolbar;
    }

    protected Fragment getInitContent() {
        return null;
    }

    protected int getContentContainerId() {
        return R.id.container;
    }

    protected DrawerLayout getDrawerLayout() {
        if (findViewById(R.id.drawer_layout) != null) {
            return (DrawerLayout) findViewById(R.id.drawer_layout);
        }
        return null;
    }

    protected int getTitleId() {
        return R.id.tvTitle;
    }

    protected boolean displayHomeButton() {
        return true;
    }

    private void addCartFragment(int cartContainerId, Fragment cartContentFragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(cartContainerId, cartContentFragment).commit();
    }

    public void showDialog(DialogFragment dialogFragment, Bundle dataBundle, String tag) {
        dialogFragment.setArguments(dataBundle);
        dialogFragment.show(getFragmentManager(), tag);
    }

    public void showDialog(Class<DialogFragment> dialogFragmentClass, Bundle dataBundle, String tag) {
        DialogFragment dialogFragment = ReflectionUtil.getInstance(dialogFragmentClass);
        dialogFragment.setArguments(dataBundle);
        dialogFragment.show(getFragmentManager(), tag);
    }

    public boolean isNetworkConnected() {
        return getApp().getNetworkManager().isConnected();
    }

    protected void post(Object event) {
        bus.post(event);
    }

    protected void startNewActivity(Class clazz) {
        startNewActivity(clazz, new Bundle());
    }

    protected void startNewActivity(Class clazz, Bundle data) {
        Intent intent = new Intent(this, clazz);
        intent.putExtras(data);
        if (data.getBoolean(Constant.FOR_RESULT)) {
            startActivityForResult(intent, data.getInt(Constant.REQUEST_CODE));
        } else {
            startActivity(intent);
        }
    }

    protected void startService(Class<? extends Service> clazz) {
        startService(new Intent(this, clazz));
    }

    public App getApp() {
        if (getApplication() instanceof App) {
            return (App) getApplication();
        } else {
            throw new RuntimeException("Please register a custom application " +
                    "class by overriding App");
        }
    }

    public void askPermission(int requestId, String... permissions) {
        List<String> needPerms = new ArrayList<>();
        for (String permission : permissions) {
            if (permission != null && !hasPermission(permission)) {
                needPerms.add(permission);
            }
        }
        ActivityCompat.requestPermissions(
                this,
                needPerms.toArray(new String[needPerms.size()]),
                requestId
        );
    }

    protected void showDialog(String message) {
        new AlertDialog.Builder(this)
                .setTitle("network error")
                .setMessage(message)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.alert_light_frame)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .show();
    }

    protected boolean hasPermission(String permission) {
        return !TextUtils.isEmpty(permission) &&
                PackageManager.PERMISSION_GRANTED ==
                        ContextCompat.checkSelfPermission(this, permission);
    }

    public void onEvent() {
    }
}
